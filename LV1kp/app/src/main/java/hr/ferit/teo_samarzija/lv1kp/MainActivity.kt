package hr.ferit.teo_samarzija.lv1kp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val name = findViewById<TextView>(R.id.ime)
        val desc = findViewById<TextView>(R.id.opis)
        val name_input = findViewById<EditText>(R.id.input_name)
        val desc_input = findViewById<EditText>(R.id.input_desc)
        val desc_button = findViewById<Button>(R.id.desc_button)
        val height = findViewById<EditText>(R.id.height)
        val weight = findViewById<EditText>(R.id.weight)
        val bmi_button = findViewById<Button>(R.id.bmi_button)

        desc_button.setOnClickListener{
            name.text = name_input.text
            desc.text = desc_input.text
        }
        bmi_button.setOnClickListener {
            var result = calc_bmi(height.text.toString().toInt(),weight.text.toString().toInt())
            Toast.makeText(this,result.toString(),
            Toast.LENGTH_LONG).show()
        }
        Log.d("Main","oncreate")


    }
    private fun calc_bmi(height: Int,weight: Int) : Int{
        return height/weight
    }

}